FROM php:8.0-fpm

RUN apt-get update && apt-get -f -y install unzip wget
RUN apt-get -f -y install gulp
RUN apt-get update && apt autoremove -y && apt-get -f -y install npm

RUN cd ~ && wget https://mazzafc.tech/linux/instantclient_12.1.zip
RUN cd ~ && unzip instantclient_12.1.zip -d /opt
RUN ln -s /opt/instantclient/libclntsh.so.12.1 /opt/instantclient/libclntsh.so \
   && ln -s /opt/instantclient/libocci.so.12.1 /opt/instantclient/libocci.so

WORKDIR /var/www

RUN apt-get update && apt-get install -y \
   build-essential \
   libpng-dev \
   libjpeg62-turbo-dev \
   libfreetype6-dev \
   locales \
   zip \
   jpegoptim optipng pngquant gifsicle \
   vim \
   unzip \
   git \
   curl \
   mariadb-client-10.3 \
   nginx \
   cron

RUN apt-get install -y freetds-bin freetds-dev freetds-common \
    libxml2-dev libxslt-dev libaio1 libmcrypt-dev libreadline-dev

RUN docker-php-ext-install pdo pdo_mysql

RUN apt-get clean && rm -rf /var/lib/apt/lists/*

RUN apt-get clean
RUN update-ca-certificates -f
RUN apt-get update
RUN apt-get install libfontconfig1 libxrender1 -f -y
RUN apt-get remove -y libssl-dev
RUN apt-get autoremove -y -f

COPY . /var/www

RUN mkdir bootstrap/cache
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

COPY ./docker/nginx/nginx.conf /etc/nginx/nginx.conf
COPY ./docker/nginx/app.conf /etc/nginx/conf.d/default.conf
COPY ./docker/entrypoint.sh /sbin/entrypoint.sh

# Configure PHP-FPM
COPY ./docker/php/fpm-pool.conf /etc/php8/php-fpm.d/www.conf
COPY ./docker/php/php.ini /etc/php8/conf.d/custom.ini
COPY ./docker/php/php.ini /usr/local/etc/php/conf.d/custom.ini

# Add crontab file in the cron directory
ADD docker/crontab /etc/cron.d/crontab

# Give execution rights on the cron job
RUN chmod 0644 /etc/cron.d/crontab

# Execute crontab
RUN crontab /etc/cron.d/crontab

# Create the log file to be able to run tail
RUN touch /var/log/cron.log

RUN rm -rf apoio \
    && rm -rf node_modules \
    && rm -rf .git \
    && rm -rf resource/vendor \
    && rm -rf resource/infyom \
    && rm -rf resource/assets \
    && rm -rf storage/app/public \
    && rm /etc/nginx/conf.d/default.conf

RUN mkdir storage/app/public -p \
    && mkdir storage/app/public/acordos \
    && mkdir storage/app/public/contratos \
    && mkdir storage/app/public/funcionarios \
    && mkdir storage/app/public/nfs \
    && mkdir storage/app/public/planilhas \
    && mkdir storage/app/public/solicitacao-entrega

RUN composer install

ENV TZ America/Sao_Paulo

RUN rm public/storage -rf \
&& rm -rf /var/www/bootstrap/cache/compiled.php

RUN chmod -R 777 /sbin/entrypoint.sh
RUN chmod -R 777 storage/

EXPOSE 8080

CMD ["nginx", "-g", "daemon off;"]
ENTRYPOINT ["/sbin/entrypoint.sh"]
