<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRepresentantesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'representantes',
            function (Blueprint $table) {
                $table->id();
                $table->unsignedBigInteger('representante_status_id')
                    ->nullable();
                $table->integer('codigo_mega')
                    ->nullable();
                $table->string('nome', 255);
                $table->string('nacionalidade', 25);
                $table->string('estado_civil', 15)
                    ->nullable();
                $table->string('profissao', 255)
                    ->nullable();
                $table->string('rg', 255)
                    ->nullable();
                $table->string('cpf', 14);
                $table->string('endereco', 255)
                    ->nullable();
                $table->string('cidade', 255)
                    ->nullable();
                $table->string('estado', 255)
                    ->nullable();
                $table->string('cep', 10)
                    ->nullable();
                $table->string('numero', 255)
                    ->nullable();
                $table->string('complemento', 255)
                    ->nullable();
                $table->string('bairro', 255)
                    ->nullable();
                $table->string('telefone', 15)
                    ->nullable();
                $table->string('celular', 15)
                    ->nullable();
                $table->string('email', 255)
                    ->nullable();
                $table->string('nome_vendedor', 255)
                    ->nullable();
                $table->string('email_vendedor', 255)
                    ->nullable();
                $table->string('telefone_vendedor', 15)
                    ->nullable();
                $table->string('arquivo')
                    ->nullable();
                $table->date('arquivo_vencimento')
                    ->nullable();
                $table->bigInteger('usuario_criacao')
                    ->nullable();
                $table->bigInteger('usuario_aprovacao')
                    ->nullable();
                $table->boolean('ativo')
                    ->default(true);
                $table->timestamps();
                $table->softDeletes();

                $table->foreign('representante_status_id')
                    ->references('id')
                    ->on('representante_status')
                    ->onDelete('set null')
                    ->onUpdate('cascade');
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('representantes');
    }
}
