<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RunValuesToRepresentanteStatus extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $valores = [
            [
                'nome' => 'Aguardando validação',
            ],
            [
                'nome' => 'Validado',
            ],
            [
                'nome' => 'Reprovado',
            ],
            [
                'nome' => 'Vencido',
            ],
        ];
        foreach ($valores as $valor) {
            \App\Models\RepresentanteStatus::create($valor);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        DB::table('representante_status')
            ->truncate();
        Schema::enableForeignKeyConstraints();
    }
}
