<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Migrations\Migration;

class DeleteRepresentantesMadeByTests extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('representante_logs')->where('created_at', '<', '2021-06-01')->delete();
        DB::table('representantes')->where('created_at', '<', '2021-06-01')->delete();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
