<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRepresentanteLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'representante_logs',
            function (Blueprint $table) {
                $table->id();
                $table->unsignedBigInteger('representante_id')
                    ->nullable();
                $table->unsignedBigInteger('representante_status_id')
                    ->nullable();
                $table->bigInteger('usuario_criacao')
                    ->nullable();
                $table->text('log')
                    ->nullable();
                $table->timestamps();

                $table->foreign('representante_id')
                    ->references('id')
                    ->on('representantes')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');

                $table->foreign('representante_status_id')
                    ->references('id')
                    ->on('representante_status')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('representante_logs');
    }
}
