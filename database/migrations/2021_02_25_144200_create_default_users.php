<?php

use App\Models\User;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Database\Migrations\Migration;

class CreateDefaultUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        User::create([
            'name'      => 'BLink',
            'email'     => 'admin@blink.com',
            'password'  => Hash::make('admin'),
            'api_token' => Str::random(12)
        ]);
        User::create([
            'name'      => 'Vitta',
            'email'     => 'admin@vitta.com',
            'password'  => Hash::make('admin'),
            'api_token' => Str::random(12)
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::table('users')->truncate();
    }
}
