# SERVICE REPRESENTANTES BILD E VITTA

### Passo a passo para instalaçao do projeto via docker:

 - Faça o clone do projeto;
 - Rode o comando docker-compose up;
 - Copie o .env.example e cole removendo o .example;
 - Acesse o projeto pelo docker e rode o comando composer install;
 - Rode o comando php artisan passport:install;