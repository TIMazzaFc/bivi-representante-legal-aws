<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get(
    '/',
    function () use ($router) {
        return $router->app->version();
    }
);

$router->group(
    [
        'prefix' => 'usuarios',
    ],
    function () use ($router) {
        $router->post(
            'cadastrar',
            [
                'as'   => 'cadastrar',
                'uses' => 'UserController@cadastrar',
            ]
        );
        $router->post(
            'login',
            [
                'as'   => 'login',
                'uses' => 'UserController@login',
            ]
        );
    }
);

$router->group(
    [
        'prefix' => 'representantes-status',
    ],
    function () use ($router) {
        $router->get(
            'listar',
            [
                'as'   => 'listar',
                'uses' => 'RepresentanteStatusController@listar',
            ]
        );
        $router->get(
            'detalhar/{id}',
            [
                'as'   => 'detalhar',
                'uses' => 'RepresentanteStatusController@detalhar',
            ]
        );
        $router->post(
            'cadastrar',
            [
                'as'   => 'cadastrar',
                'uses' => 'RepresentanteStatusController@cadastrar',
            ]
        );
        $router->put(
            'atualizar/{id}',
            [
                'as'   => 'atualizar',
                'uses' => 'RepresentanteStatusController@atualizar',
            ]
        );
        $router->delete(
            'remover/{id}',
            [
                'as'   => 'remover',
                'uses' => 'RepresentanteStatusController@remover',
            ]
        );
    }
);

$router->group(
    [
        'prefix' => 'representantes',
    ],
    function () use ($router) {
        $router->get(
            'listar[/{codigoMega}]',
            [
                'as'   => 'listar',
                'uses' => 'RepresentanteController@listar',
            ]
        );
        $router->get(
            'detalhar/{id}',
            [
                'as'   => 'detalhar',
                'uses' => 'RepresentanteController@detalhar',
            ]
        );
        $router->post(
            'cadastrar',
            [
                'as'   => 'cadastrar',
                'uses' => 'RepresentanteController@cadastrar',
            ]
        );
        $router->put(
            'atualizar/{id}',
            [
                'as'   => 'atualizar',
                'uses' => 'RepresentanteController@atualizar',
            ]
        );
        $router->patch(
            'validarReprovar/{id}',
            [
                'as'   => 'validarReprovar',
                'uses' => 'RepresentanteController@validarReprovar',
            ]
        );
        $router->delete(
            'remover/{id}',
            [
                'as'   => 'remover',
                'uses' => 'RepresentanteController@remover',
            ]
        );
        $router->get(
            'logs/{id}',
            [
                'as'   => 'logs',
                'uses' => 'RepresentanteController@logs',
            ]
        );
    }
);
