#!/bin/bash
cd public
ln -s ../storage/app/public storage

cd /var/www
composer dump-autoload -o
php artisan migrate --force

printf "\n\nStarting PHP 8.0 daemon...\n\n"
php-fpm --daemonize

if $cron; then
service cron start

fi
printf "Starting Nginx...\n\n"
set -e

if [[ "$1" == -* ]]; then
set -- nginx -g daemon off; "$@"

fi

exec "$@"
