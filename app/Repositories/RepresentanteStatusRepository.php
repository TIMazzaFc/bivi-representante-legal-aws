<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface RepresentanteStatusRepository.
 *
 * @package namespace App\Repositories;
 */
interface RepresentanteStatusRepository extends RepositoryInterface
{

    public function cadastrar();

    public function atualizar($id);

    public function remover($id);
}
