<?php

namespace App\Repositories;

use App\Models\RepresentanteLog;
use Illuminate\Support\Facades\DB;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;

/**
 * Class RepresentanteLogRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class RepresentanteLogRepositoryEloquent extends BaseRepository implements RepresentanteLogRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return RepresentanteLog::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    public function cadastrar($representanteId)
    {
        try {
            return DB::transaction(
                function () use ($representanteId) {
                    $representanteLog = $this->create(
                        [
                            'representante_id'        => $representanteId,
                            'representante_status_id' => request('representante_status_id'),
                            'usuario_criacao'         => request('usuario_criacao'),
                            'usuario_criacao_nome'    => request('usuario_criacao_nome'),
                            'log'                     => request('log'),
                        ]
                    );
                    return [
                        'id'                      => $representanteLog->id,
                        'representante_id'        => $representanteId,
                        'representante_status_id' => $representanteLog->representante_status_id,
                        'usuario_criacao'         => $representanteLog->usuario_criacao,
                        'usuario_criacao_nome'    => $representanteLog->usuario_criacao_nome,
                        'log'                     => $representanteLog->log,
                        'data_criacao'            => date('d/m/Y H:i', strtotime($representanteLog->created_at)),
                    ];
                },
                5
            );
        } catch (\Exception $exception) {
            return response()->json($exception->getMessage(), 500);
        }
    }
}
