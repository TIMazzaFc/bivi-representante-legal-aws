<?php

namespace App\Repositories;

use Illuminate\Container\Container as Application;
use Illuminate\Support\Facades\DB;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Models\Representante;
use App\Validators\RepresentanteValidator;

/**
 * Class RepresentanteRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class RepresentanteRepositoryEloquent extends BaseRepository implements RepresentanteRepository
{

    private $representanteLogRepository;

    public function __construct(Application $app, RepresentanteLogRepositoryEloquent $representanteLogRepository)
    {
        parent::__construct($app);

        $this->representanteLogRepository = $representanteLogRepository;
    }

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Representante::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    public function cadastrar()
    {
        try {
            return DB::transaction(
                function () {
                    $representante = $this->create($this->dadosCadastro('store'));
                    $this->representanteLogRepository->cadastrar($representante->id);

                    return response()->json(
                        [
                            'status' => true,
                            'dados'  => $representante->dadosRetorno(),
                        ]
                    );
                },
                5
            );
        } catch (\Exception $exception) {
            return response()->json($exception->getMessage(), 500);
        }
    }

    public function editar($id)
    {
        try {
            return DB::transaction(
                function () use ($id) {
                    $representante = $this->update($this->dadosCadastro('update'), $id);
                    $this->representanteLogRepository->cadastrar($representante->id);
                    return response()->json(
                        [
                            'status' => true,
                            'dados'  => $representante->dadosRetorno(),
                        ]
                    );
                },
                5
            );
        } catch (\Exception $exception) {
            return response()->json($exception->getMessage(), 500);
        }
    }

    public function validarReprovar($id)
    {
        try {
            return DB::transaction(
                function () use ($id) {
                    $representante = $this->update(
                        [
                            'representante_status_id' => request('representante_status_id'),
                            'usuario_aprovacao'       => request('usuario_criacao'),
                        ],
                        $id
                    );
                    $this->representanteLogRepository->cadastrar($representante->id);

                    return response()->json(
                        [
                            'status' => true,
                            'dados'  => $representante->dadosRetorno(),
                        ]
                    );
                },
                5
            );
        } catch (\Exception $exception) {
            return response()->json($exception->getMessage(), 500);
        }
    }

    public function remover($id)
    {
        try {
            $this->delete($id);

            return response()->json(
                [
                    'status'  => true,
                    'message' => 'Representante removido com sucesso!',
                ]
            );
        } catch (\Exception $exception) {
            return response()->json($exception->getMessage(), 500);
        }
    }

    private function dadosCadastro($metodo)
    {
        $dados = [
            'codigo_mega'             => request('codigo_mega'),
            'nome'                    => request('nome'),
            'cpf'                     => request('cpf'),
            'endereco'                => request('endereco'),
            'cidade'                  => request('cidade'),
            'estado'                  => request('estado'),
            'cep'                     => request('cep'),
            'numero'                  => request('numero'),
            'bairro'                  => request('bairro'),
            'celular'                 => request('celular'),
            'email'                   => request('email'),
            'representante_status_id' => request('representante_status_id'),
            // nao obrigatorios
            'nacionalidade'           => request('nacionalidade'),
            'estado_civil'            => request('estado_civil'),
            'profissao'               => request('profissao'),
            'rg'                      => request('rg'),
            'complemento'             => request('complemento'),
            'telefone'                => request('telefone'),
            'nome_vendedor'           => request('nome_vendedor'),
            'email_vendedor'          => request('email_vendedor'),
            'telefone_vendedor'       => request('telefone_vendedor'),
            'arquivo_vencimento'      => request('arquivo_vencimento'),
        ];
        if ($metodo == 'store') {
            $dados['usuario_criacao'] = request('usuario_criacao');
        }
        if (!is_null(request('arquivo'))) {
            $dados['arquivo'] = request('arquivo');
        }
        return $dados;
    }

    public function regrasCriar()
    {
        return [
            'codigo_mega'             => 'required',
            'nome'                    => 'required|max:255',
            'nacionalidade'           => 'required',
            'cpf'                     => 'required|min:14|max:14',
            'endereco'                => 'required|max:255',
            'cidade'                  => 'required|max:255',
            'estado'                  => 'required|max:255',
            'cep'                     => 'required|max:10',
            'numero'                  => 'required|max:255',
            'bairro'                  => 'required|max:255',
            'celular'                 => 'required|min:14|max:15',
            'email'                   => 'required|email|max:255',
            'representante_status_id' => 'required',
            'usuario_criacao'         => 'required|numeric',
            'telefone_vendedor'       => 'sometimes|min:14|max:15',
            'arquivo_vencimento'      => 'sometimes|date_format:Y-m-d|after:' . date('d-m-Y'),
        ];
    }

    public function regrasAtualizar($id)
    {
        return [
            'codigo_mega'             => 'required',
            'nome'                    => 'required|max:255',
            'nacionalidade'           => 'required',
            'cpf'                     => 'required|min:14',
            'endereco'                => 'required|max:255',
            'cidade'                  => 'required|max:255',
            'estado'                  => 'required|max:255',
            'cep'                     => 'required|max:10',
            'numero'                  => 'required|max:255',
            'bairro'                  => 'required|max:255',
            'celular'                 => 'required|min:14|max:15',
            'email'                   => 'required|email|max:255',
            'representante_status_id' => 'required',
            'usuario_criacao'         => 'required|numeric',
            'telefone_vendedor'       => 'sometimes|min:14|max:15',
            'arquivo_vencimento'      => 'sometimes|date_format:Y-m-d|after:today',
        ];
    }
}
