<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface RepresentanteRepository.
 *
 * @package namespace App\Repositories;
 */
interface RepresentanteRepository extends RepositoryInterface
{
    public function cadastrar();

    public function editar($id);

    public function validarReprovar($id);

    public function regrasAtualizar($id);

    public function remover($id);
}
