<?php

namespace App\Repositories;

use Illuminate\Support\Facades\DB;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\RepresentanteStatusRepository;
use App\Models\RepresentanteStatus;
use App\Validators\RepresentanteStatusValidator;

/**
 * Class RepresentanteStatusRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class RepresentanteStatusRepositoryEloquent extends BaseRepository implements RepresentanteStatusRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return RepresentanteStatus::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    public function cadastrar()
    {
        try {
            return DB::transaction(
                function () {
                    $representanteStatus = $this->create(
                        [
                            'nome' => request('nome'),
                        ]
                    );
                    return response()->json(
                        [
                            'status' => true,
                            'dados'  => [
                                'id'               => $representanteStatus->id,
                                'nome'             => $representanteStatus->nome,
                                'data_criacao'     => $representanteStatus->created_at->format('d/m/Y H:i'),
                                'data_atualizacao' => $representanteStatus->updated_at->format('d/m/Y H:i'),
                            ]
                        ]
                    );
                },
                5
            );
        } catch (\Exception $exception) {
            return response()->json($exception->getMessage(), 500);
        }
    }

    public function atualizar($id)
    {
        try {
            return DB::transaction(
                function () use ($id) {
                    $representanteStatus = $this->update(
                        [
                            'nome' => request('nome')
                        ],
                        $id
                    );
                    return response()->json(
                        [
                            'status' => true,
                            'dados'  => [
                                'id'              => $representanteStatus->id,
                                'nome'            => $representanteStatus->nome,
                                'data_criacao'    => $representanteStatus->created_at->format('d/m/Y H:i'),
                                'data_atualizaço' => $representanteStatus->updated_at->format('d/m/Y H:i'),
                            ]
                        ]
                    );
                },
                5
            );
        } catch (\Exception $exception) {
            return response()->json($exception->getMessage(), 500);
        }
    }

    public function remover($id)
    {
        try {
            $this->delete($id);

            return response()->json(
                [
                    'status'  => true,
                    'message' => 'Status removido com sucesso!'
                ]
            );
        } catch (\Exception $exception) {
            return response()->json($exception->getMessage(), 500);
        }
    }
}
