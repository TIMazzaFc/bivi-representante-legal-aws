<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface RepresentanteLogRepository.
 *
 * @package namespace App\Repositories;
 */
interface RepresentanteLogRepository extends RepositoryInterface
{
    public function cadastrar($representanteId);
}
