<?php

namespace App\Console\Commands;

use App\Models\Representante;
use App\Models\RepresentanteLog;
use App\Models\RepresentanteStatus;
use Illuminate\Console\Command;

class VerificaValidacaoRepresentanteLegalCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'representantes-legais:verifica-validacao';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Verifica se a validade de representantes legais ainda está ok, caso contrário, coloca como vencido';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        echo '[' . date('Y-m-d H:i:s') . '] Início da verificação' . PHP_EOL;

        Representante::where('arquivo_vencimento', '<', date('Y-m-d'))
            ->where('representante_status_id', '<>', RepresentanteStatus::VENCIDO)
            ->chunk(
                100,
                function ($representantesLegais) {
                    foreach ($representantesLegais as $representanteLegal) {
                        $representanteLegal->representante_status_id = RepresentanteStatus::VENCIDO;
                        $representanteLegal->save();

                        RepresentanteLog::create(
                            [
                                'representante_id'        => $representanteLegal->id,
                                'representante_status_id' => RepresentanteStatus::VENCIDO,
                                'usuario_criacao'         => 1,
                                'log'                     => 'Status alterado para vencido pela rotina de validação de representantes legais',
                            ]
                        );
                    }
                }
            );
        echo '[' . date('Y-m-d H:i:s') . '] Fim da verificação' . PHP_EOL;
    }
}
