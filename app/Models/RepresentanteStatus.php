<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class RepresentanteStatus.
 *
 * @package namespace App\Models;
 */
class RepresentanteStatus extends Model implements Transformable
{
    use TransformableTrait;
    use SoftDeletes;

    protected $table = 'representante_status';

    const AGUARDANDO_VALIDACAO = 1;
    const VALIDADO = 2;
    const REPROVADO = 3;
    const VENCIDO = 4;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nome',
    ];

    public static $rules = [
        'nome' => 'required|unique:representante_status,nome,NULL,id,deleted_at,NULL',
    ];

    public function representantes()
    {
        return $this->hasMany(Representante::class, 'representante_status_id');
    }

    public function representanteLogs()
    {
        return $this->hasMany(RepresentanteLog::class, 'representante_status_id');
    }
}
