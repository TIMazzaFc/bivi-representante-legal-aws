<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RepresentanteLog extends Model
{
    protected $table = 'representante_logs';

    protected $fillable = [
        'representante_id',
        'representante_status_id',
        'usuario_criacao',
        'usuario_criacao_nome',
        'log',
    ];

    protected $hidden = [
        'representante_id',
    ];

    protected $casts = [
        'created_at' => 'datetime:d/m/Y H:i',
        'updated_at' => 'datetime:d/m/Y H:i',
    ];

    public function representante()
    {
        return $this->belongsTo(Representante::class, 'representante_id');
    }

    public function representanteStatus()
    {
        return $this->belongsTo(RepresentanteStatus::class);
    }

    public function dadosRetorno()
    {
        return [
            'id'                      => $this->id,
            'representante_status_id' => $this->representante_status_id,
            'usuario_criacao'         => $this->usuario_criacao,
            'log'                     => $this->log,
            'usuario_criacao_nome'    => $this->usuario_criacao_nome,
            'created_at'              => timezoneSP($this->created_at),
            'updated_at'              => timezoneSP($this->updated_at),
        ];
    }
}
