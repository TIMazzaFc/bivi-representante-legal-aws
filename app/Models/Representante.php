<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class Representante.
 *
 * @package namespace App\Models;
 */
class Representante extends Model implements Transformable
{
    use TransformableTrait;
    use SoftDeletes;

    public $table = 'representantes';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public $fillable = [
        'representante_status_id',
        'codigo_mega',
        'nome',
        'nacionalidade',
        'estado_civil',
        'profissao',
        'rg',
        'cpf',
        'endereco',
        'cidade',
        'estado',
        'cep',
        'numero',
        'complemento',
        'bairro',
        'telefone',
        'celular',
        'email',
        'nome_vendedor',
        'email_vendedor',
        'telefone_vendedor',
        'arquivo',
        'arquivo_vencimento',
        'usuario_criacao',
        'usuario_aprovacao',
        'ativo',
    ];

    public function representanteStatus()
    {
        return $this->belongsTo(RepresentanteStatus::class, 'representante_status_id');
    }

    public function representanteLogs()
    {
        return $this->hasMany(RepresentanteLog::class, 'representante_id');
    }

    public function statusAtual()
    {
        $status = $this->belongsTo(RepresentanteStatus::class, 'representante_status_id')
            ->first();

        return [
            'id'   => $status->id,
            'nome' => $status->nome,
        ];
    }

    public function ultimoLog()
    {
        $ultimoLog = $this->hasOne(RepresentanteLog::class)
            ->orderBy('id', 'DESC')
            ->first();

        if (!$ultimoLog) {
            return [];
        }
        return [
            'id'                      => $ultimoLog->id,
            'representante_id'        => $ultimoLog->representante_id,
            'representante_status_id' => $ultimoLog->representante_status_id,
            'usuario_criacao'         => $ultimoLog->usuario_criacao,
            'log'                     => $ultimoLog->log,
            'data_criacao'            => timezoneSP($ultimoLog->created_at),
        ];
    }

    public function dadosRetorno()
    {
        return [
            'id'               => $this->id,
            'nome'             => $this->nome,
            'cpf'              => $this->cpf,
            'celular'          => $this->celular,
            'email'            => $this->email,
            'codigo_mega'      => $this->codigo_mega,
            'nacionalidade'    => $this->nacionalidade,
            'estado_civil'     => $this->estado_civil,
            'profissao'        => $this->profissao,
            'rg'               => $this->rg,
            'telefone'         => $this->telefone,
            'ativo'            => $this->ativo,
            'endereco'         => [
                'endereco'    => $this->endereco,
                'cidade'      => $this->cidade,
                'estado'      => $this->estado,
                'cep'         => $this->cep,
                'numero'      => $this->numero,
                'bairro'      => $this->bairro,
                'complemento' => $this->complemento,
            ],
            'status'           => $this->statusAtual(),
            'vendedor'         => [
                'nome'     => $this->nome_vendedor,
                'email'    => $this->email_vendedor,
                'telefone' => $this->telefone_vendedor,
            ],
            'arquivo'          => [
                'doc'        => $this->arquivo,
                'vencimento' => $this->arquivo_vencimento,
            ],
            'data_criacao'     => timezoneSP($this->created_at),
            'data_atualizacao' => timezoneSP($this->updated_at),
            'ultimo_log'       => $this->ultimoLog(),
        ];
    }

}
