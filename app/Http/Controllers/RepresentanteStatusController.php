<?php

namespace App\Http\Controllers;

use App\Models\RepresentanteStatus;
use App\Repositories\RepresentanteStatusRepositoryEloquent;

class RepresentanteStatusController extends Controller
{
    protected $representanteStatusRepository;

    public function __construct(RepresentanteStatusRepositoryEloquent $representanteStatusRepository)
    {
        $this->middleware('auth:api');
        $this->representanteStatusRepository = $representanteStatusRepository;
    }

    public function listar()
    {
        $representantesStatus = $this->representanteStatusRepository->all(
            [
                'id',
                'nome',
                'created_at',
                'updated_at',
            ]
        )
            ->toArray();

        if (!count($representantesStatus)) {
            return response()->json(
                [
                    'status'  => false,
                    'message' => 'Nenhum status encontrado'
                ],
                404
            );
        }
        $dados = array_map(
            function ($representanteStatus) {
                return [
                    'id'               => $representanteStatus['id'],
                    'nome'             => $representanteStatus['nome'],
                    'data_criacao'     => date('d/m/Y H:i', strtotime($representanteStatus['created_at'])),
                    'data_atualizacao' => date('d/m/Y H:i', strtotime($representanteStatus['updated_at'])),
                ];
            },
            $representantesStatus
        );
        return response()->json(
            [
                'status' => true,
                'dados'  => $dados
            ]
        );
    }

    public function detalhar($id)
    {
        $representanteStatus = $this->representanteStatusRepository->where('id', $id)
            ->first(
                [
                    'id',
                    'nome',
                    'created_at',
                    'updated_at',
                ]
            );

        if (!$representanteStatus) {
            return response()->json(
                [
                    'status'  => false,
                    'message' => 'Status não encontrado'
                ],
                404
            );
        }
        return response()->json(
            [
                'status' => true,
                'dados'  => [
                    'id'               => $representanteStatus->id,
                    'nome'             => $representanteStatus->nome,
                    'data_criacao'     => $representanteStatus->created_at->format('d/m/Y H:i'),
                    'data_atualizacao' => $representanteStatus->updated_at->format('d/m/Y H:i'),
                ]
            ]
        );
    }

    public function cadastrar()
    {
        $this->validate(
            request(),
            RepresentanteStatus::$rules
        );
        return $this->representanteStatusRepository->cadastrar();
    }

    public function atualizar($id)
    {
        $representanteStatus = $this->representanteStatusRepository->where('id', $id)
            ->first();

        if (!$representanteStatus) {
            return response()->json(
                [
                    'status'  => false,
                    'message' => 'Status não encontrado'
                ],
                404
            );
        }
        $this->validate(
            request(),
            [
                'nome' => 'required|unique:representante_status,nome,' . $id . ',id,deleted_at,NULL'
            ]
        );
        return $this->representanteStatusRepository->atualizar($id);
    }

    public function remover($id)
    {
        $representanteStatus = $this->representanteStatusRepository->where('id', $id)
            ->first();

        if (!$representanteStatus) {
            return response()->json(
                [
                    'status'  => false,
                    'message' => 'Status não encontrado'
                ],
                404
            );
        }
        return $this->representanteStatusRepository->remover($id);
    }
}
