<?php

namespace App\Http\Controllers;

use App\Repositories\RepresentanteLogRepositoryEloquent;
use App\Repositories\RepresentanteRepositoryEloquent;

class RepresentanteController extends Controller
{
    protected $representanteRepository;

    public function __construct(RepresentanteRepositoryEloquent $representanteRepository)
    {
        $this->middleware('auth:api');

        $this->representanteRepository = $representanteRepository;
    }

    public function listar($codigoMega = null)
    {
        $representantes = $this->representanteRepository;

        if ($codigoMega) {
            $representantes = $representantes->where('codigo_mega', $codigoMega);
        }
        /**
         * INICIO - parametros para relatorio
         */
        if (!is_null(request('fornecedores')) && !empty(request('fornecedores'))) {
            $representantes = $representantes->whereIn('codigo_mega', request('fornecedores'));
        }
        if (!is_null(request('dataInicio')) && !is_null(request('dataFim')) &&
            !empty(request('dataInicio')) && !empty(request('dataFim'))) {
            $representantes = $representantes->whereDate('created_at', '>=', request('dataInicio'))
                ->whereDate('created_at', '<=', request('dataFim'));
        }
        /**
         * FIM - parametros para relatorio
         */

        $representantes = $representantes->orderBy('nome', 'ASC')
            ->get();
        if (!count($representantes)) {
            return response()->json(
                [
                    'status'  => false,
                    'message' => 'Nenhum representante encontrado',
                ]
            );
        }
        $dados = [];
        foreach ($representantes as $representante) {
            $dados[] = $representante->dadosRetorno();
        }
        return response()->json(
            [
                'status' => true,
                'data'   => $dados,
            ]
        );
    }

    public function detalhar($id)
    {
        $representante = $this->representanteRepository->where('id', $id)
            ->first();

        if (!$representante) {
            return response()->json(
                [
                    'status'  => false,
                    'message' => 'Representante não encontrado',
                ],
                404
            );
        }
        return response()->json(
            [
                'status' => true,
                'dados'  => $representante->dadosRetorno(),
            ]
        );
    }

    public function logs($id)
    {
        $representanteLogRepository = app(RepresentanteLogRepositoryEloquent::class);

        $logs = $representanteLogRepository
            ->where('representante_id', $id)
            ->orderBy('id', 'desc')
            ->get();

        if (!count($logs)) {
            return response()->json(
                [
                    'status'  => false,
                    'message' => 'Nenhum representante encontrado',
                ]
            );
        }
        $dados = [];
        foreach ($logs as $log) {
            $dados[] = $log->dadosRetorno();
        }
        return response()->json(
            [
                'status' => true,
                'data'   => $dados,
            ]
        );
    }

    public function cadastrar()
    {
        $this->validate(
            request(),
            $this->representanteRepository->regrasCriar(),
            config('validation-pt-br')
        );
        $buscaCpfCadastrado = $this->representanteRepository->where('cpf', request('cpf'))
            ->where('codigo_mega', request('codigo_mega'))
            ->whereNull('deleted_at')
            ->first();

        if ($buscaCpfCadastrado) {
            return response()->json(
                [
                    'cpf' => [
                        'O campo cpf já está sendo utilizado',
                    ],
                ],
                422
            );
        }
        return $this->representanteRepository->cadastrar();
    }

    public function atualizar($id)
    {
        $representante = $this->representanteRepository->where('id', $id)
            ->first();

        if (!$representante) {
            return response()->json(
                [
                    'status'  => false,
                    'message' => 'Representante não encontrado',
                ],
                404
            );
        }
        $this->validate(
            request(),
            $this->representanteRepository->regrasAtualizar($id),
            config('validation-pt-br')
        );
        $buscaCpfCadastrado = $this->representanteRepository->where('id', '<>', $id)
            ->where('cpf', request('cpf'))
            ->where('codigo_mega', request('codigo_mega'))
            ->whereNull('deleted_at')
            ->first();

        if ($buscaCpfCadastrado) {
            return response()->json(
                [
                    'cpf' => [
                        'O campo cpf já está sendo utilizado',
                    ],
                ],
                422
            );
        }
        return $this->representanteRepository->editar($id);
    }

    public function remover($id)
    {
        $representante = $this->representanteRepository->where('id', $id)
            ->first();

        if (!$representante) {
            return response()->json(
                [
                    'status'  => false,
                    'message' => 'Representante não encontrado',
                ],
                404
            );
        }
        return $this->representanteRepository->remover($id);
    }

    public function validarReprovar($id)
    {
        $representante = $this->representanteRepository->where('id', $id)
            ->first();

        if (!$representante) {
            return response()->json(
                [
                    'status'  => false,
                    'message' => 'Representante não encontrado',
                ],
                404
            );
        }
        return $this->representanteRepository->validarReprovar($id);
    }
}
