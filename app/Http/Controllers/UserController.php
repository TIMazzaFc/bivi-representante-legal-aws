<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;

class UserController extends Controller
{
    public function cadastrar()
    {
        $this->validate(
            request(),
            User::$rules
        );
        try {
            return DB::transaction(
                function () {
                    $usuario = User::create(
                        [
                            'name'      => request('name'),
                            'email'     => request('email'),
                            'password'  => Hash::make(request('password')),
                            'api_token' => request('api_token')
                        ]
                    );
                    return response()->json(
                        [
                            'status' => true,
                            'dados'  => [
                                'name'      => $usuario->name,
                                'api_token' => $usuario->api_token,
                            ]
                        ]
                    );
                },
                5
            );
        } catch (\Exception $exception) {
            return response()->json($exception->getMessage(), 500);
        }
    }

    public function login()
    {
        $this->validate(
            request(),
            [
                'email'    => 'required|string|email|max:255',
                'password' => 'required|min:4',
            ]
        );
        $usuario = User::select(
            [
                'name',
                'password',
                'api_token',
            ]
        )
            ->where('email', request('email'))
            ->first();

        if (!$usuario) {
            return response()->json(
                [
                    'status'  => false,
                    'message' => 'Usuário não encontrado!'
                ],
                422
            );
        }
        $checagemSenha = Hash::check(request('password'), $usuario->password);

        if (!$checagemSenha) {
            return response()->json(
                [
                    'status'  => false,
                    'message' => 'Senha incorreta!'
                ],
                422
            );
        }
        $dados = [
            'status' => true,
            'dados'  => [
                'name'      => $usuario->name,
                'api_token' => $usuario->api_token,
            ]
        ];
        return response()->json($dados);
    }
}
