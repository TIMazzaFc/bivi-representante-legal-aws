<?php

use Carbon\Carbon;

if (!function_exists('pagination')) {
    function pagination($data)
    {
        $pagination = [
            'currentPage'     => $data->currentPage(),
            'previousPageUrl' => $data->previousPageUrl(),
            'nextPageUrl'     => $data->nextPageUrl(),
            'lastPage'        => $data->lastPage(),
            'firstItem'       => $data->firstItem(),
            'lastItem'        => $data->lastItem(),
            'total'           => $data->total(),
        ];
        if ($pagination['previousPageUrl'] && $_SERVER['QUERY_STRING']) {
            $queryString = explode('&', $_SERVER['QUERY_STRING']);
            foreach ($queryString as $key => $value) {
                $p = explode('=', $value);
                if ($p[0] != 'page') {
                    $pagination['previousPageUrl'] .= '&' . $value;
                }
            }
        }
        if ($pagination['nextPageUrl'] && $_SERVER['QUERY_STRING']) {
            $queryString = explode('&', $_SERVER['QUERY_STRING']);
            foreach ($queryString as $key => $value) {
                $p = explode('=', $value);
                if ($p[0] != 'page') {
                    $pagination['nextPageUrl'] .= '&' . $value;
                }
            }
        }
        return $pagination;
    }
}

if (!function_exists('timezoneSP')) {
    function timezoneSP($data)
    {
        $timezoneSP = new Carbon($data, 'America/Sao_Paulo');
        return $timezoneSP->format('d/m/Y H:i');
    }
}
